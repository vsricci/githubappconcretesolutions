//
//  User.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 01/11/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable {
    
    var login : String?
    var avatarURL : String?
    
    
    
    required init?(map: Map) {
        
        if map.JSON["login"] == nil {
            return nil
        }
        if map.JSON["avatar_url"] == nil {
            return nil
        }
        
    }
    
    func mapping(map: Map) {
        
        login <- map["login"]
        avatarURL <- map["avatar_url"]
    }

}
