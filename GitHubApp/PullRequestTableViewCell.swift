//
//  PullRequestTableViewCell.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 01/11/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
class PullRequestTableViewCell: UITableViewCell {
    
    var imageUser : UIImageView = UIImageView()
    var nameUser : UILabel = UILabel()
    var titleRepository : UILabel = UILabel()
    var dateRepository : UILabel = UILabel()
    var bodyRepository : UILabel = UILabel()
    var nameFullUser : UILabel = UILabel()
    var createdDate : UILabel = UILabel()
    var createdDateLabel : UILabel = UILabel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
       
        

        initLayout()
        // Configure the view for the selected state
    }
    
    
    func initLayout() {
        
        titleRepository.textColor = UIColor.blue
        titleRepository.font = UIFont.systemFont(ofSize: 12)
        titleRepository.textAlignment = .left
        titleRepository.numberOfLines = 2
        
        
        bodyRepository.textColor = UIColor.black
        bodyRepository.textAlignment = .left
        bodyRepository.font = UIFont.systemFont(ofSize: 12)
        bodyRepository.numberOfLines = 3
        
        
        imageUser.contentMode = .scaleAspectFit
        imageUser.layer.cornerRadius = 12
        
        
    
        nameUser.textAlignment = .left
        nameUser.font = UIFont.systemFont(ofSize: 12)
        nameUser.textColor = UIColor.black
        
        nameFullUser.textAlignment = .left
        nameFullUser.font = UIFont.systemFont(ofSize: 12)
        nameFullUser.textColor = UIColor.black
        
        createdDateLabel.textAlignment = .left
        createdDateLabel.font = UIFont.boldSystemFont(ofSize: 10)
        createdDateLabel.numberOfLines = 0
        createdDateLabel.textColor = UIColor.gray
        createdDateLabel.text = "Criado em:"
        
        
        createdDate.textAlignment = .right
        createdDate.font = UIFont.systemFont(ofSize: 12)
        createdDate.numberOfLines = 0
        createdDate.textColor = UIColor.black
        
        
        
        imageUser.translatesAutoresizingMaskIntoConstraints = false
        nameUser.translatesAutoresizingMaskIntoConstraints = false
        titleRepository.translatesAutoresizingMaskIntoConstraints = false
        dateRepository.translatesAutoresizingMaskIntoConstraints = false
        bodyRepository.translatesAutoresizingMaskIntoConstraints = false
        nameFullUser.translatesAutoresizingMaskIntoConstraints = false
        createdDateLabel.translatesAutoresizingMaskIntoConstraints = false
        createdDate.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(imageUser)
        self.contentView.addSubview(nameUser)
        self.contentView.addSubview(titleRepository)
        self.contentView.addSubview(dateRepository)
        self.contentView.addSubview(bodyRepository)
        self.contentView.addSubview(nameFullUser)
        self.contentView.addSubview(createdDateLabel)
        self.contentView.addSubview(createdDate)

        
        
        titleRepository.snp.makeConstraints { (make) in
            
            make.top.equalTo(self.contentView.snp.top).offset(10)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            make.height.equalTo(40)
            
        }
        
        bodyRepository.snp.makeConstraints { (make) in
            make.top.equalTo(titleRepository.snp.top).offset(40)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            //make.bottom.equalTo(self.imageUser.snp.top).offset(30)
           // make.height.equalTo(100)
            
        }
        
        
        imageUser.snp.makeConstraints { (make) in
            make.top.equalTo(bodyRepository.snp.top).offset(50)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.width.height.equalTo(48)
           // make.bottom.equalTo(self.contentView.snp.bottom).offset(-5)
            
        }
        
        nameUser.snp.makeConstraints { (make) in
            
            make.top.equalTo(imageUser.snp.top)
            make.leading.equalTo(imageUser.snp.trailing).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(10)
            make.height.equalTo(20)
        }
       
        
        nameFullUser.snp.makeConstraints { (make) in
            
            make.top.equalTo(nameUser.snp.bottom)
            make.leading.equalTo(imageUser.snp.trailing).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(10)
            make.height.equalTo(20)
            //make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        createdDateLabel.snp.makeConstraints { (make) in
            
            make.top.equalTo(imageUser.snp.top).offset(60)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
        
        createdDate.snp.makeConstraints { (make) in
            
            make.top.equalTo(imageUser.snp.top).offset(60)
            make.leading.equalTo(createdDateLabel.snp.trailing).offset(10)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
            
        }

        
    }

}
