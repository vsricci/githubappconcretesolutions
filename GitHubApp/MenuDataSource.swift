//
//  MenuDataSource.swift
//  GitHubApp
//
//  Created by Vinicius on 04/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit

class MenuDataSource: NSObject, UITableViewDataSource {

    
    weak var tableView : UITableView?
    weak var dataSource : PullRequestDataSource?
    var listPullRepositorys : [String] = ["Repositorios"]
    
    required init(tableView: UITableView, listPullRepositorys: [String]) {
        
        self.tableView = tableView
        self.listPullRepositorys = listPullRepositorys
        
        self.tableView?.register(MenuTableViewCell.self, forCellReuseIdentifier: "cellMenu")
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return listPullRepositorys.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMenu", for: indexPath) as! MenuTableViewCell
        
        cell.nameItem.text = listPullRepositorys[indexPath.row]
        
        return cell
    }
    
    
    func initTableView() {
        
        self.tableView?.translatesAutoresizingMaskIntoConstraints = false
        
        let view = ["tableView" : self.tableView]
        
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tableView]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
    }
}
