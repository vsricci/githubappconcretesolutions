//
//  RepositoriosTableViewCell.swift
//  GitHubApp
//
//  Created by Vinicius on 30/10/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
class RepositoriosTableViewCell: UITableViewCell {
    
    
    var nameRepository : UILabel = UILabel()
    var descriptionRepository : UILabel = UILabel()
    var numberOfForks : UILabel = UILabel()
    var imageForks : UIImageView = UIImageView()
    var numberOfStars : UILabel = UILabel()
    var imageStars : UIImageView = UIImageView()
    var imageUser : UIImageView = UIImageView()
    var nameUser : UILabel = UILabel()
    var nameFullUser : UILabel = UILabel()
    var buttonSave : UIButton = UIButton()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
        
        initCells()
        
        
    }
    
    
    func initCells() {
        
        
        
        
        nameRepository.textColor = UIColor.blue
        nameRepository.font = UIFont.boldSystemFont(ofSize: 17)
        nameRepository.textAlignment = .left
        nameRepository.numberOfLines = 0
        
        descriptionRepository.textAlignment = .left
        descriptionRepository.font = UIFont.systemFont(ofSize: 12)
        descriptionRepository.numberOfLines = 5
        
        
        
        imageUser.contentMode = .scaleAspectFit
        imageUser.layer.cornerRadius = 12
        imageUser.clipsToBounds = true
        
        imageForks.image =  UIImage(named: "fork")
        imageForks.contentMode = .scaleAspectFit
        imageForks.tintColor = UIColor.orange
        imageForks.clipsToBounds = false
        
        numberOfForks.textAlignment = .center
        numberOfForks.font = UIFont.systemFont(ofSize: 14)
        numberOfForks.textColor = UIColor.orange
        
        
        imageStars.tintColor = UIColor.orange
        imageStars.contentMode = .scaleAspectFit
        imageStars.image = UIImage(named: "star")
        
        numberOfStars.textAlignment = .center
        numberOfStars.font = UIFont.systemFont(ofSize: 14)
        numberOfStars.textColor = UIColor.orange
        
        
        nameUser.textAlignment = .center
        nameUser.font = UIFont.systemFont(ofSize: 12)
        
        
        nameFullUser.textAlignment = .left
        
        nameFullUser.font = UIFont.systemFont(ofSize: 12)
        nameFullUser.numberOfLines = 2
        
        buttonSave.setImage(UIImage(named: "save"), for: .normal)
        buttonSave.tintColor = UIColor.red
        
        
        
        
        
        nameRepository.translatesAutoresizingMaskIntoConstraints = false
        descriptionRepository.translatesAutoresizingMaskIntoConstraints = false
        numberOfForks.translatesAutoresizingMaskIntoConstraints = false
        imageForks.translatesAutoresizingMaskIntoConstraints = false
        numberOfForks.translatesAutoresizingMaskIntoConstraints = false
        imageStars.translatesAutoresizingMaskIntoConstraints = false
        imageUser.translatesAutoresizingMaskIntoConstraints = false
        nameUser.translatesAutoresizingMaskIntoConstraints = false
        nameFullUser.translatesAutoresizingMaskIntoConstraints = false
        buttonSave.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.contentView.addSubview(nameRepository)
        self.contentView.addSubview(descriptionRepository)
        self.contentView.addSubview(numberOfForks)
        self.contentView.addSubview(imageForks)
        self.contentView.addSubview(numberOfStars)
        self.contentView.addSubview(imageStars)
        self.contentView.addSubview(imageUser)
        self.contentView.addSubview(nameUser)
        self.contentView.addSubview(nameFullUser)
        self.contentView.addSubview(buttonSave)
                
        nameRepository.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView.snp.top).offset(10)
            //make.height.equalTo(23)
            make.leading.equalTo(self.contentView.snp.leading).offset(20)
            make.trailing.equalTo(self.contentView.snp.trailing)
        }
        
        descriptionRepository.snp.makeConstraints { (make) in
            
            make.top.equalTo(nameRepository.snp.bottom).offset(10)
            make.leading.equalTo(self.contentView.snp.leading).offset(20)
            //make.height.equalTo(60)
            make.trailing.equalTo(imageUser.snp.leading)
        }
        
        
        imageUser.snp.makeConstraints { (make) in
            make.leading.equalTo(descriptionRepository.snp.trailing)
            make.top.equalTo(self.contentView.snp.top).offset(60)
            make.width.height.equalTo(48)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
        }
        
        imageForks.snp.makeConstraints { (make) -> Void in
            //make.top.equalTo(descriptionRepository.snp.bottom).offset(20)
            make.width.height.equalTo(15)
            make.leading.equalTo(self.contentView.snp.leading).offset(20)
            make.bottom.equalToSuperview().offset(-6)
            
        }
        
        numberOfForks.snp.makeConstraints { (make) in
            
           // make.top.equalTo(imageForks.snp.top)
           // make.baseline.equalTo(imageForks.snp.baseline)
            make.bottom.equalToSuperview().offset(-6)
            make.leading.equalTo(imageForks.snp.trailing).offset(5)
            make.trailing.equalTo(imageStars.snp.leading).offset(-10)
        }
        
        
        imageStars.snp.makeConstraints { (make) in
            
            make.width.height.equalTo(15)
            make.leading.equalTo(numberOfForks.snp.trailing)
            //make.top.equalTo(imageForks.snp.top)
            make.bottom.equalToSuperview().offset(-6)
        }
        
        
        numberOfStars.snp.makeConstraints { (make) in
           // make.top.equalTo(imageForks.snp.top)
            make.baseline.equalTo(imageForks.snp.baseline)
            make.leading.equalTo(imageStars.snp.trailing).offset(10)
            make.bottom.equalToSuperview().offset(-6)
           // make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
        }
        
        
        nameUser.snp.makeConstraints { (make) in
            make.top.equalTo(imageUser.snp.top).offset(50)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
           // make.leading.equalTo(self.contentView.snp.leading)
            //make.right.equalTo(10)
            //make.bottom.equalTo(numberOfStars.snp.top)
        }
        
        
        nameFullUser.snp.makeConstraints { (make) in
            make.top.equalTo(nameUser.snp.bottomMargin).offset(5)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            
        }
        
        
        buttonSave.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(10)
            make.width.height.equalTo(15)
            
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-30)
            make.bottom.equalTo(nameRepository.snp.top).offset(-10)
            
        }
        
        
        
        
        
      /*  let views = ["nameRepository" : nameRepository, "descriptionRepository" : descriptionRepository, "numberOfForks" : numberOfForks, "imageForks": imageForks, "numberOfStars" : numberOfStars, "imageStars": imageForks, "imageUser" : imageUser, "nameUser" : nameUser, "contentView": contentView] as [String : Any]
        
        let metrics = ["imageForks": 60
        ]
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[nameRepository]-10-|", options: [], metrics: nil, views: views))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(-75)-[nameRepository]-|", options: [], metrics: nil, views: views))
        
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[descriptionRepository]-60-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views))
        
       // NSLayoutConstraint.constraints(withVisualFormat: "H:|-[descriptionRepository]-|", options: [.alignAllTrailing, .alignAllLeading], metrics: nil, views: views)
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[descriptionRepository]-|", options: [], metrics: nil, views: views))
        
        
       // NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imageForks]-|", options: [], metrics: nil, views: views))
        
        //NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[imageForks]-|", options: [], metrics: nil, views: views))
        
        //NSLayoutConstraint.init(item: imageForks, attribute: .top, relatedBy: .equal, toItem: descriptionRepository, attribute: .bottom, multiplier: 1, constant: 60)
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[imageForks]-|", options: .alignAllTop, metrics: metrics, views: views))*/
        
    }

}
