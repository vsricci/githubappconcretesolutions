//
//  RepositoriosTableViewDataSource.swift
//  GitHubApp
//
//  Created by Vinicius on 30/10/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import SDWebImage
import  AlamofireNetworkActivityIndicator




final class RepositoriosTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate{
    
    let show = ViewController()
    weak var tableView : UITableView?
    var listaDeRepositorios : [Items] = []
    weak var delegate : UITableViewDelegate?
     var urlPrimeiroEndPoint = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"
    
     var listsNameRepositorys : [String] = []
     var listsImageUser : [String] = []
     var listsNameUser : [String] = []
     var listsFullNameUser : [String] = []
    
    
    weak var indicator : UIActivityIndicatorView?
    var rowsCount = 10
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var valor = 0
        
        if rowsCount > listaDeRepositorios.count {
            
            valor = listaDeRepositorios.count
        }
            
        else {
            valor = rowsCount
        }
        return valor
    }
    
    
    
    required init(listaRepositorios: [Items], tableView : UITableView) {
        
        self.listaDeRepositorios = listaRepositorios
        self.tableView = tableView
       
        self.tableView?.register(RepositoriosTableViewCell.self, forCellReuseIdentifier: "cell")
        
       
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RepositoriosTableViewCell
        
        let item = self.listaDeRepositorios[indexPath.row]
        
       
        cell.nameRepository.text = item.nameRepository
        cell.descriptionRepository.text = item.descriptionRepository
        cell.numberOfForks.text = String(describing: item.numbersOfForks)
        cell.imageUser.sd_setImage(with: URL(string: (item.owner2?.pictureUser)!), placeholderImage: UIImage(named: ""), options: .continueInBackground, completed: nil)
        cell.nameUser.text = item.owner2?.nameLogin
        cell.nameFullUser.text = item.nameFullUser
        cell.numberOfStars.text = String(describing: item.numbersOfStars)
        cell.buttonSave.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(salvaRepositorio(sender:))))
        return cell
        
    }
    
    
    func salvaRepositorio(sender : UITapGestureRecognizer) {
        
        
        
        print("botao clicado")
        
        let index = sender.location(in: tableView)
        
        if let indexPath = tableView?.indexPathForRow(at: index) {
            
            let item = listaDeRepositorios[indexPath.row]
            
            listsNameRepositorys.append(item.nameRepository!)
            listsImageUser.append((item.owner2?.pictureUser)!)
            listsNameUser.append((item.owner2?.nameLogin)!)
            listsFullNameUser.append(item.nameFullUser!)
            
            
            UserDefaults.standard.set(listsNameRepositorys, forKey: "nomeRepositorio")
            UserDefaults.standard.set(listsImageUser, forKey: "imagemUsuarioRepositorio")
            UserDefaults.standard.set(listsNameUser, forKey: "nomeUsuario")
            UserDefaults.standard.set(listsFullNameUser, forKey: "nomeInteiroUsuario")
            
        }
    }
    
    
       
    

    
    
    
    func indicatorNetwork() {
        
        self.indicator?.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            
            self.tableView?.isHidden = true
            
            
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0, execute: {
                
                
                NetworkActivityIndicatorManager.shared.startDelay = 5.0
                NetworkActivityIndicatorManager.shared.isNetworkActivityIndicatorVisible
                
                NetworkActivityIndicatorManager.shared.completionDelay = 4.5
                
                self.indicator?.stopAnimating()
                self.tableView?.isHidden = false
                
                self.initTableView()
                
            })
            
            
        }
    }
    
    
    
    
    func initTableView() {
        
        
        
        
        self.tableView?.translatesAutoresizingMaskIntoConstraints = false
        //self.tableView?.backgroundColor = UIColor.gray
        
        
        self.tableView?.rowHeight = 160
        
        
        let view = ["tableView" : self.tableView]
        
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tableView]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
       
        
    }
    
        
  
    func addItems(items: Items) {
        
        self.listaDeRepositorios.append( items)
        self.tableView?.reloadData()
    }

    
    
}



