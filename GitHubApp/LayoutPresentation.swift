//
//  LayoutPresentation.swift
//  GitHubApp
//
//  Created by Vinicius on 03/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
class LayoutPresentation: UIView {
    
    
    var indicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var viewFundo : UIView = UIView()
    var labelSituacao : UILabel = UILabel()
    
    func initLayout(texto: String){
        
        
        viewFundo.translatesAutoresizingMaskIntoConstraints = false
        labelSituacao.translatesAutoresizingMaskIntoConstraints = false
        
        viewFundo.addSubview(labelSituacao)
        viewFundo.addSubview(indicator)
        self.addSubview(viewFundo)
        
        
        indicator.activityIndicatorViewStyle = .gray
        indicator.center = self.center
        indicator.startAnimating()
        
        
        labelSituacao.textAlignment = .center
        labelSituacao.font = UIFont.systemFont(ofSize: 20)
        labelSituacao.text = texto
        labelSituacao.textColor = UIColor.black
        
        viewFundo.backgroundColor = UIColor.white
        
        
        
        viewFundo.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.leading.equalTo(self.snp.leading)
            make.trailing.equalTo(self.snp.trailing)
            make.bottom.equalTo(self.snp.bottom)
            
        }
        
        
        indicator.snp.makeConstraints { (make) in
            
            make.center.equalTo(self.snp.center)
            
        }
        
        
        labelSituacao.snp.makeConstraints { (make) in
            
            make.top.equalTo(indicator.snp.bottom)
            make.centerX.equalTo(self.snp.centerX)
            make.height.equalTo(60)
        }

    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
