//
//  MenuTableViewCell.swift
//  GitHubApp
//
//  Created by Vinicius on 04/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit

class MenuTableViewCell: UITableViewCell {
    
    var nameItem : UILabel = UILabel()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        initCells()
    }
    
    
    func initCells() {
        
        
        
        
        nameItem.textColor = UIColor.blue
        nameItem.font = UIFont.boldSystemFont(ofSize: 17)
        nameItem.textAlignment = .center
        nameItem.numberOfLines = 0
        
        nameItem.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        self.contentView.addSubview(nameItem)
        
        
        nameItem.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView.snp.top).offset(10)
            //make.height.equalTo(23)
            make.leading.equalTo(self.contentView.snp.leading).offset(20)
            make.trailing.equalTo(self.contentView.snp.trailing)
        }
        
    }

}
