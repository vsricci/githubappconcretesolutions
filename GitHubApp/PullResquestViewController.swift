//
//  PullResquestViewController.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 01/11/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import  AlamofireObjectMapper
import Result
import Reusable
import SDWebImage
import  ObjectMapper

class PullResquestViewController: UIViewController, UITableViewDelegate {
    
    var pullDataSource : PullRequestDataSource!
    var tableView : UITableView = UITableView()
    var listPullRepositorys : [Pull] = []
    var userName = String()
    var repositoryString = String()
    var layoutPresentation = LayoutPresentation()
    var urlFormated : String?
    var urlRquestPull = "https://api.github.com/repos/<criador>/<repositório>/pulls"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) { 
            
            self.layoutPresentation.initLayout(texto: "Carregando Pull Repositórios")
        }
        
        
        
        
        self.navigationItem.title = "Pull"
        
        print(userName)
        print(repositoryString)
         urlFormated = "https://api.github.com/repos/\(userName)/\(repositoryString)/pulls"
        
        
        print(urlFormated)
        
        
        pullDataSource = PullRequestDataSource(tableView: tableView, listPullRepositorys: self.listPullRepositorys)
        
        tableView.dataSource = pullDataSource
        tableView.delegate = self
        
        self.view.addSubview(tableView)
        
        pullDataSource.initTableView()
        requestPull()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = pullDataSource.listPullRepositorys[indexPath.row]
        
        openPull(url: item.htmlURL!)
    }
    
    
    func openPull(url : String){
        
        let vc = PullRequestSelectedViewController()
        vc.urlPull = url
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func requestPull() {
        
        
        
        Alamofire.request(urlFormated!).responseJSON { (response) in
            
            let json = response.result.value as? [[String: AnyObject]]
            switch(response.result) {
                
            case .success:
                
                var items = Mapper<Pull>().mapArray(JSONArray: json!)
                
                
                
                for item in items {
                    
                    DispatchQueue.main.async {
                        
                        self.pullDataSource?.listPullRepositorys.append(item)
                        self.pullDataSource.tableView?.reloadData()
                        
                    }
                }
                
                
                
                break
            case .failure:
                
                
                break
                
                
                
            }
            
        }.validate()
    }
    

    
}
