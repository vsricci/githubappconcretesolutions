//
//  FavoritesRepositorysDataSource.swift
//  GitHubApp
//
//  Created by Vinicius on 05/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit

class FavoritesRepositorysDataSource: NSObject, UITableViewDataSource {
    
    weak var tableView : UITableView?
    var listsFavoritesRepositorys : [RepositorioSalvo] = []
    
    var listsNameRepositorys : [String] = []
    var listsImageUser : [String] = []
    var listsNameUser : [String] = []
    var listsFullNameUser : [String] = []
    
    required init(tableView : UITableView) {
        self.tableView = tableView
        
        self.tableView?.register(FavoritesRepositorysTableViewCell.self, forCellReuseIdentifier: "cellFavorite")
    }
    
    func initTableView() {
        
        self.tableView?.translatesAutoresizingMaskIntoConstraints = false
        //self.tableView?.backgroundColor = UIColor.gray
        
        
        self.tableView?.rowHeight = 160
        
        
        /*let view = ["tableView" : self.tableView]
        
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tableView]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        */
        
    }
    
    
    func mostraDados() -> Bool {
        
        var sucess = false
        
        if UserDefaults.standard.object(forKey: "nomeRepositorio") != nil && UserDefaults.standard.object(forKey: "imagemUsuarioRepositorio") != nil && UserDefaults.standard.object(forKey: "nomeUsuario") != nil && UserDefaults.standard.object(forKey: "nomeInteiroUsuario") != nil {
        
        
        listsNameRepositorys =  (UserDefaults.standard.object(forKey: "nomeRepositorio") as? [String])!
        listsImageUser = (UserDefaults.standard.object(forKey: "imagemUsuarioRepositorio") as? [String]!)!
        listsNameUser = (UserDefaults.standard.object(forKey: "nomeUsuario") as? [String]!)!
        listsFullNameUser = (UserDefaults.standard.object(forKey: "nomeInteiroUsuario") as? [String]!)!
        
        
        
        if listsNameRepositorys.count > 0 && listsImageUser.count > 0 && listsFullNameUser.count > 0 && listsNameUser.count > 0 {
            
            
            sucess = true
            var contador = 0
            
            
            for item in listsNameRepositorys {
            
                let saveRepository  = RepositorioSalvo(nameUser: item, imageUser: listsImageUser[contador], fullNameUser: listsFullNameUser[contador], nameRepository: listsNameRepositorys[contador])
                
                
                listsFavoritesRepositorys.append(saveRepository)
                contador+=1
            }
            
            
            
        }
        else {
            
            sucess = false
        }
        
            
        }
       
        
        return sucess
        
    }

    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFavorite", for: indexPath) as! FavoritesRepositorysTableViewCell
        
        let item = listsFavoritesRepositorys[indexPath.row]
        
        cell.nameRepository.text = item.nameRepository
        cell.nameUser.text = item.nameUser
        cell.imageUser.sd_setImage(with: URL(string: item.imageUser), placeholderImage: UIImage(named: ""), options: .continueInBackground, completed: nil)
        cell.fullName.text = item.fullNameUser
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listsFavoritesRepositorys.count
    }
}
