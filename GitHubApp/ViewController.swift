//
//  ViewController.swift
//  GitHubApp
//
//  Created by Vinicius on 29/10/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import  SideMenu



class ViewController: UIViewController, UITableViewDelegate{
    
    var botaoMenu : UIButton = UIButton()
    var tableView : UITableView = UITableView()
    var dataSource : RepositoriosTableViewDataSource!
    var delegate : UITableViewDelegate?
    var service = Service()
    var listaRepositorios : [Items] = []
    var urlPrimeiroEndPoint = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"
    var indicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var viewFundo : UIView = UIView()
   // var labelSituacao : UILabel = UILabel()*/
    var layoutPresentation = LayoutPresentation()
    var verifyConnection = InternetOn()
    var pageInit : Int = 1
    var botaoClose : UIButton = UIButton()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationItem.title = "GitHub"
      
        
        
        layoutPresentation.initLayout(texto: "Carregando Repositórios")
        
        if verifyConnection
            .verificaConexao() == true {
            
            self.requesRepositorio(page: 1)
        }
        else {
            
            let tentarNovamente = UIAlertAction(title: "Tentar Novamente", style: .default, handler: { (UIAlertAction) in
                
                self.verifyConnection.verificaConexao()
                
            })
            
            let cancel = UIAlertAction(title: "Cancelar", style: .destructive, handler: { (UIAlertAction) in
                
                
            })
            
            let alertVC = UIAlertController(title: "Sem Conexão", message: "Verifique sua conexão de Internet ...", preferredStyle: .actionSheet)
            
            alertVC.addAction(tentarNovamente)
            alertVC.addAction(cancel)
            self.present(alertVC, animated: true, completion: nil)
        }
       
        botaoClose.backgroundColor = UIColor.red
        botaoClose.translatesAutoresizingMaskIntoConstraints = false
        botaoClose.addTarget(self, action: #selector(close), for: .touchUpInside)
        self.view.addSubview(botaoClose)
        
        
        botaoClose.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(5)
            make.width.height.equalTo(20)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            
        }
        
        
    }
    
    
    func close(){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func connected() {
    
        dataSource = RepositoriosTableViewDataSource(listaRepositorios: self.listaRepositorios, tableView: tableView)
        
        
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        
        
        indicator.activityIndicatorViewStyle = .gray
        
        
        
        view.addSubview(tableView)
        dataSource.initTableView()
        
       
        

        
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("teste")
        let item = dataSource.listaDeRepositorios[indexPath.row]
        
        showClass(item: item)
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
        dataSource.tableView?.deselectRow(at: indexPath, animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            
            
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            
            
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
                print(self.dataSource.rowsCount + self.dataSource.listaDeRepositorios.count)
                spinner.stopAnimating()
                
                print("\(self.dataSource.rowsCount)" + " " +  "\(self.dataSource.listaDeRepositorios.count)")
                if self.dataSource.rowsCount > self.dataSource.listaDeRepositorios.count {
                    
                    spinner.stopAnimating()
                    
                    print("\(self.dataSource.rowsCount)" + " " +  "\(self.dataSource.listaDeRepositorios.count)")
                    let label  = UILabel()
                    
                  
                    self.dataSource.tableView?.tableFooterView?.isHidden = false
                    
                    self.pageInit+=1
                    
                    self.requesRepositorio(page: self.pageInit)
                    print(self.pageInit)
                }
                else {
                    self.dataSource.rowsCount += 10
                    self.dataSource.tableView?.reloadData()
                }
                
                
            })
            
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: (self.dataSource.tableView?.bounds.width)!, height: CGFloat(44))
            self.dataSource.tableView?.tableFooterView = spinner
            self.dataSource.tableView?.tableFooterView?.isHidden = false
            
            
        }

        
        
        
    }
    

    
    func requesRepositorio(page : Int) {
        
        
        let params:[String:AnyObject?] = ["page": page as AnyObject]
        
        Alamofire.request(urlPrimeiroEndPoint, parameters : params).responseObject { (response: DataResponse<Repositorio>) in
            
            switch (response.result) {
                
            case.success :
                
                self.indicator.stopAnimating()
                self.viewFundo.isHidden = true
                self.connected()
                
                let repository = response.result.value
                
                if let teste = repository?.items {
                    
                    
                    
                    
                    for item in teste {
                        
                        
                        DispatchQueue.main.async {
                            self.dataSource.addItems(items: item)
                            
                            
                        }
                    
                    }
                }
                break
            case.failure :
                
                break
                
            }
            
            
            }
      
        
    }
    
  
    func showClass(item: Items) {
        
        
        let navController = UISideMenuNavigationController()
        
        
        var pullRepositorysVC: PullResquestViewController = PullResquestViewController()
      
        
        pullRepositorysVC.userName = (item.owner2?.nameLogin!)!
        pullRepositorysVC.repositoryString = item.nameRepository!
        
        self.present(pullRepositorysVC, animated: true, completion: nil)
        
       
        
    }
    
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

