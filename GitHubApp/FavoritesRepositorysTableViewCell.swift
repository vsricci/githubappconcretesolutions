//
//  FavoritesRepositorysTableViewCell.swift
//  GitHubApp
//
//  Created by Vinicius on 05/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
class FavoritesRepositorysTableViewCell: UITableViewCell {
    
    var nameRepository : UILabel = UILabel()
    var imageUser : UIImageView = UIImageView()
    var nameUser : UILabel = UILabel()
    var fullName : UILabel = UILabel()
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        initCells()
    }
    
    func initCells() {
        
        nameRepository.translatesAutoresizingMaskIntoConstraints = false
        imageUser.translatesAutoresizingMaskIntoConstraints = false
        nameUser.translatesAutoresizingMaskIntoConstraints = false
        fullName.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(nameRepository)
        self.contentView.addSubview(imageUser)
        self.contentView.addSubview(nameUser)
        self.contentView.addSubview(fullName)
        
        
        
        nameUser.textColor = UIColor.black
        
        
        imageUser.snp.makeConstraints { (make) in
            make.width.height.equalTo(48)
            make.top.equalTo(self.contentView.snp.top).offset(20)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
        }
        
        nameUser.snp.makeConstraints { (make) in
            make.top.equalTo(imageUser.snp.top).offset(60)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            
        }
        
        fullName.snp.makeConstraints { (make) in
            make.top.equalTo(nameUser.snp.top).offset(20)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            
        }
        
        nameRepository.snp.makeConstraints { (make) in
            make.top.equalTo(fullName.snp.top).offset(20)
            make.leading.equalTo(self.contentView.snp.leading).offset(10)
            make.trailing.equalTo(self.contentView.snp.trailing).offset(-10)
            
        }
        
    }
    

}
