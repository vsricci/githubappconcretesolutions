//
//  Head.swift
//  GitHubApp
//
//  Created by Vinicius on 02/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper
class Head: NSObject, Mappable {
    
    var repo : Repo?
    
    required init?(map: Map) {
        
        
       /* if let repo : Repo = map["repo"].value() {
            
            self.repo = repo
        }
        
        else {
            return nil
        }
        
        if map.JSON["repo"] == nil {
            return nil
        }*/
        
    }
    
    func mapping(map: Map) {
        
        repo <- map["repo"]
    }

}
