//
//  GitHubAppUITests.swift
//  GitHubAppUITests
//
//  Created by Vinicius on 29/10/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import XCTest

@testable import GitHubApp
class GitHubAppUITests: XCTestCase {
        
  
    func testRequesRepositorio() {
        
        let value = 1
        let page = value
        let success = true
        
        XCTAssertEqual(page, 1)
        XCTAssertEqual(success, true)
        
    }
    
    
    func testsShowClass() {
        
        var userName : String?
        var repositoryString : String?
        
        XCTAssertNil(userName)
        XCTAssertNil(repositoryString)
        
        XCTAssertEqual(userName, userName)
        XCTAssertEqual(repositoryString, repositoryString)
    }
    
    
    func testHelloWord() {
        
        var helloWord : String?
        
        XCTAssertNil(helloWord)
        
        helloWord = "teste"
        XCTAssertEqual(helloWord, "teste")
    }
    
}
