//
//  InitViewController.swift
//  GitHubApp
//
//  Created by Vinicius on 04/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import  SnapKit
import  SideMenu
class InitViewController: UIViewController {
    
    var texto : UILabel = UILabel()
    var gitHubLabel : UILabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        texto.text = "Desafio Concrete Solutions"
        texto.textAlignment = .center
        texto.textColor = UIColor.red
        texto.font = UIFont.boldSystemFont(ofSize: 20)
        
        gitHubLabel.text = "GitHub App"
        gitHubLabel.textAlignment = .center
        gitHubLabel.textColor = UIColor.red
        gitHubLabel.font = UIFont.systemFont(ofSize: 15)
        
        
        
        
        texto.translatesAutoresizingMaskIntoConstraints = false
        gitHubLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.view.addSubview(gitHubLabel)
        self.view.addSubview(texto)
        
        
        texto.snp.makeConstraints { (make) in
            make.center.equalTo(self.view.snp.center)
            
        }
        
        gitHubLabel.snp.makeConstraints { (make) in
            make.top.equalTo(texto.snp.top).offset(60)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
        let menuLeft = UISideMenuNavigationController(rootViewController: RepositorysSaveViewController())
        SideMenuManager.menuLeftNavigationController = menuLeft
        
        
        SideMenuManager.menuAddPanGestureToPresent(toView: (self.navigationController?.navigationBar)!)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: (self.navigationController?.view)!)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "iconeMenu"), style: .plain, target: self, action: #selector(opeMenu))
    }

    
    func opeMenu () {
        
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
