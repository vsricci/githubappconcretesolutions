//
//  Pull.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 01/11/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper


class Pull: NSObject, Mappable {

     var title : String?
     var body : String?
     var createdDate : Date?
     var user : User?
     var head : Head?
     var htmlURL : String?
    
    required init?(map: Map) {
        
        
        if let body: String = map["body"].value() {
            
            self.body = body
        }
        else {
            
            return nil
        }
        
        if map.JSON["head"] == nil {
            
            return nil
        }
              
        
        if map.JSON["title"] == nil {
            return nil
        }
        if map.JSON["created_at"] == nil {
            return nil
        }
      
        if map.JSON["user"] == nil {
            return nil
        }
        
        if map.JSON["html_url"] == nil {
            return nil
        }
        
        
    }
    
    func mapping(map: Map) {
        
        title <- map["title"]
        user <- map["user"]
        body <- map["body"]
        createdDate <- (map["created_at"], DateTransform())
        htmlURL <- map["html_url"]
        head <- map["head"]
        
        
    }
    
    
    
}
