//
//  PullRequestDataSource.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 01/11/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
import Reusable

class PullRequestDataSource: NSObject, UITableViewDataSource {

    weak var tableView : UITableView?
    weak var dataSource : PullRequestDataSource?
    var listPullRepositorys : [Pull] = []
    
    required init(tableView: UITableView, listPullRepositorys: [Pull]) {
        
        self.tableView = tableView
        self.listPullRepositorys = listPullRepositorys
        
        self.tableView?.register(PullRequestTableViewCell.self, forCellReuseIdentifier: "cellPull")
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPull", for: indexPath) as! PullRequestTableViewCell
        
        
        let item = self.listPullRepositorys[indexPath.row]
        
        
        
        cell.titleRepository.text = messagesDefault(item: item).0
        cell.nameUser.text = messagesDefault(item: item).1
        cell.nameFullUser.text = messagesDefault(item: item).2
        cell.imageUser.sd_setImage(with: URL(string: (messagesDefault(item: item).3)), placeholderImage: UIImage(named: ""), options: .continueInBackground, completed: nil)
        cell.bodyRepository.text = messagesDefault(item: item).4
        cell.createdDate.text = convertDateToString(date: item.createdDate!)
        
        
        return cell
    }
    
    
    func convertDateToString(date : Date) -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MMM d, yyyy"
        let newDate = dateFormatter.string(from: date)
        print(newDate)
        
        return newDate
    }
    
    
    func messagesDefault(item : Pull) -> (String, String, String, String, String) {
        var texto = String()
        
        
        if item.title == ""  {
            
            item.title = "Sem Informações"
        }
        else {
            item.title = item.title!
        }
        if item.head?.repo?.name == ""  {
            
            item.head?.repo?.name = "Sem Informações"
        }
        else {
            item.head?.repo?.name = item.head?.repo?.name
        }
        if item.head == nil  {
            
            item.head?.repo?.fullName = "Sem Informações"
        }
        else {
            item.head?.repo?.fullName = item.head?.repo?.fullName!
        }
        
        if item.body == "" || item.body == nil {
            
            item.body  = "Sem Informações"
        }
        else {
            item.body = item.body!
        }
        if item.user?.avatarURL == ""  {
            
            item.user?.avatarURL = "user_default"
        }
        else {
            item.user?.avatarURL = (item.user?.avatarURL!)!
        }
        
        return (item.title!, (item.user!.login)!, (item.head?.repo?.fullName)!, (item.user?.avatarURL)!, item.body!)
    }
    
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.listPullRepositorys.count
    }
    
    
    func initTableView() {
        
        self.tableView?.translatesAutoresizingMaskIntoConstraints = false
        //self.tableView?.backgroundColor = UIColor.gray
        
        
        
        
       // self.tableView?.rowHeight = 50
        
        
        let view = ["tableView" : self.tableView]
        
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[tableView]-0-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tableView]-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: view))
        
       
        
        
    }
    func addItems(items:Pull) {
        
        self.listPullRepositorys.append(items)
        self.tableView?.reloadData()
    }
    
    
  
}
