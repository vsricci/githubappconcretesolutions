//
//  Repo.swift
//  GitHubApp
//
//  Created by Vinicius on 02/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper
class Repo: NSObject,Mappable {
    
    var name : String?
    var fullName : String?
    
    required init?(map: Map) {
        
        
        
        if  map.JSON["name"] == nil {
            
            return nil
        }
        if map.JSON["full_name"] == nil {
            return nil
        }
        
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        fullName <- map["full_name"]
    }

}
