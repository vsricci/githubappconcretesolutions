//
//  RepositorioSalvo.swift
//  GitHubApp
//
//  Created by Vinicius on 05/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit

class RepositorioSalvo: NSObject {
    
    var nameUser : String!
    var imageUser : String!
    var fullNameUser : String!
    var nameRepository : String!
    
    
     init(nameUser : String, imageUser: String, fullNameUser : String, nameRepository : String) {
        
        self.nameUser = nameUser
        self.imageUser = imageUser
        self.fullNameUser = fullNameUser
        self.nameRepository = nameRepository
    }

}
