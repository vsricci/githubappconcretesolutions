//
//  Items.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 31/10/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import  ObjectMapper


class Items: NSObject, Mappable {
    
    var nameRepository: String?
    var descriptionRepository : String?
    var numbersOfStars: Int?
    var numbersOfForks : Int?
    var owner2 : Owner?
    var nameFullUser : String?
    
    required init?(map: Map) {
        
        
        if let numbersOfStars : Int = map["stargazers_count"].value() {
            
            self.numbersOfStars = numbersOfStars
        }
        else {
            
            return nil
        }
        
        
        if map.JSON["name"] == nil {
            return nil
        }
        if map.JSON["full_name"] == nil {
            return nil
        }
        if map.JSON["description"] == nil {
            return nil
        }
        if map.JSON["stargazers_count"] == nil {
            return nil
        }
        if map.JSON["forks_count"] == nil {
            return nil
        }
        if map.JSON["owner"] == nil {
            return nil
        }
    }
    
    func mapping(map: Map) {
        
        nameRepository <- map["name"]
        nameFullUser <- map["full_name"]
        descriptionRepository <- map["description"]
        numbersOfStars <- map["stargazers_count"]
        numbersOfForks <- map["forks_count"]
        owner2 <- map["owner"]
        
    }
    


}
