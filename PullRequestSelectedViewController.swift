//
//  PullRequestSelectedViewController.swift
//  GitHubApp
//
//  Created by Vinicius on 04/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import  SnapKit
class PullRequestSelectedViewController: UIViewController {
    
    
    var webView : UIWebView = UIWebView()
    
    var urlPull : String = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        
        initWebView()
        // Do any additional setup after loading the view.
    }

    
    func initWebView() {
        
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(webView)
        
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
            
        }
        
        
        webView.loadRequest(URLRequest(url: URL(string: urlPull)!))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
