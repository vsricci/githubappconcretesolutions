//
//  RepositorysSaveViewController.swift
//  GitHubApp
//
//  Created by Vinicius on 04/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import  SnapKit
class RepositorysSaveViewController: UIViewController, UITableViewDelegate {

    var tableView : UITableView = UITableView()
    var dataSource : MenuDataSource?
    var lists : [String] = ["Repositórios", "Pulls Favoritados"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        dataSource = MenuDataSource(tableView: tableView, listPullRepositorys: lists)
        
        
       
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        self.view.addSubview(tableView)
        
        dataSource?.initTableView()
        
        
        
        
       
        

        // Do any additional setup after loading the view.
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = lists[indexPath.row]
        
        
        itemsMenu(item: item)
        
    }
    
    
    func itemsMenu(item : String) {
        
        
        switch item {
        case "Repositórios":
            
            let repositorysVC : ViewController = ViewController()
            
            self.present(repositorysVC, animated: true, completion: nil)
            
        case "Pulls Favoritados":
            let pullRepositorsVC : FavoritesRepositorysViewController = FavoritesRepositorysViewController()
            
            self.present(pullRepositorsVC, animated: true, completion: nil)
        default:
            
            print("sem item")
        }
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
