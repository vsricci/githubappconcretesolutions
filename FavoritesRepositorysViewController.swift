//
//  FavoritesRepositorysViewController.swift
//  GitHubApp
//
//  Created by Vinicius on 05/11/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import SnapKit
class FavoritesRepositorysViewController: UIViewController, UITableViewDelegate {

    
    var tableView : UITableView = UITableView()
    var viewNotRepositorysSaved : UIView = UIView()
    var viewRepositorysSaved : UIView = UIView()
    var labelSemRepositorios : UILabel = UILabel()
    var botaoClose : UIButton = UIButton()
    var dataSource : FavoritesRepositorysDataSource!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        dataSource = FavoritesRepositorysDataSource(tableView: tableView)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        view.addSubview(viewRepositorysSaved)
        view.addSubview(viewNotRepositorysSaved)
        
        if dataSource.mostraDados() == true {
            
            
            viewRepositorysSaved.isHidden = false
            viewNotRepositorysSaved.isHidden = true
            initTableView()
        }
        
        else {
            viewRepositorysSaved.isHidden = true
            viewNotRepositorysSaved.isHidden = false
            initLayout()
        }
        
        
    
    }
    
    
    
    func close() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func initTableView() {
        
        viewRepositorysSaved.translatesAutoresizingMaskIntoConstraints = false
        botaoClose.translatesAutoresizingMaskIntoConstraints = false
        
        viewRepositorysSaved.addSubview(tableView)
        viewRepositorysSaved.addSubview(botaoClose)
        
        viewRepositorysSaved.backgroundColor = UIColor.white
        
        botaoClose.backgroundColor = UIColor.red
        botaoClose.addTarget(self, action: #selector(close), for: .touchUpInside)
        
        viewRepositorysSaved.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
            
        }
        
        
        botaoClose.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.top.equalTo(viewRepositorysSaved.snp.top).offset(10)
            make.leading.equalTo(viewRepositorysSaved.snp.leading).offset(10)
            
        }
        
        tableView.snp.makeConstraints({ (make) in
            make.top.equalTo(botaoClose.snp.top).offset(30)
            make.leading.equalTo(viewRepositorysSaved.snp.leading)
            make.trailing.equalTo(viewRepositorysSaved.snp.trailing)
            make.bottom.equalTo(viewRepositorysSaved.snp.bottom)

        })
        
        
       
        
        dataSource.initTableView()
    }
    
    
    func initLayout() {
        
        viewNotRepositorysSaved.translatesAutoresizingMaskIntoConstraints = false
        labelSemRepositorios.translatesAutoresizingMaskIntoConstraints = false
        botaoClose.translatesAutoresizingMaskIntoConstraints = false
        
        
        botaoClose.backgroundColor = UIColor.blue
        botaoClose.addTarget(self, action: #selector(close), for: .touchUpInside)
        
        viewNotRepositorysSaved.backgroundColor = UIColor.white
        viewNotRepositorysSaved.addSubview(labelSemRepositorios)
        viewNotRepositorysSaved.addSubview(botaoClose)
        
        
        labelSemRepositorios.text = "Sem Repositórios Salvos"
        labelSemRepositorios.textAlignment = .center
        labelSemRepositorios.textColor = UIColor.black
        labelSemRepositorios.font = UIFont.boldSystemFont(ofSize: 20)
        
        viewNotRepositorysSaved.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.leading.equalTo(self.view.snp.leading)
            make.trailing.equalTo(self.view.snp.trailing)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        
        labelSemRepositorios.snp.makeConstraints { (make) in
           
            make.center.equalTo(self.viewNotRepositorysSaved.snp.center)
            
        }
        
        botaoClose.snp.makeConstraints { (make) in
            make.top.equalTo(viewNotRepositorysSaved.snp.top).offset(40)
            make.leading.equalTo(viewNotRepositorysSaved.snp.leading).offset(10)
            make.width.height.equalTo(24)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
