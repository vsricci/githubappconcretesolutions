//
//  Owner.swift
//  GitHubApp
//
//  Created by MegaleiosWeb01 on 31/10/17.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper
class Owner: NSObject, Mappable {

    
    var pictureUser : String?
    var nameLogin : String?
    
    required init?(map: Map) {
        
        if map.JSON["avatar_url"] == nil {
            return nil
        }
        if map.JSON["login"] == nil {
            
        }
    }
    
    func mapping(map: Map) {
        
        pictureUser <- map["avatar_url"]
        nameLogin <- map["login"]
        
    }
}
