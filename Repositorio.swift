//
//  Usuario.swift
//  GitHubApp
//
//  Created by Vinicius on 30/10/2017.
//  Copyright © 2017 Vinicius. All rights reserved.
//

import UIKit
import  ObjectMapper


class Repositorio: Mappable {
    
    var totalCount : Int?
    var incomplete_results : Bool?
    var items : [Items]?
    var pulls : [Pull]?

    
    required init?(map: Map) {
        
        if let totalCount : Int = map["total_count"].value() {
            
            self.totalCount = totalCount
        }
        else {
            
            return nil
        }
        
       /* if map.JSON["total_count"] == nil {
            return nil
        }
        if map.JSON["incomplete_results"] == nil {
            return nil
        }
        if map.JSON["items"] == nil {
            return nil
        }
        if map.JSON["user"] == nil {
            return nil
        }*/
        
    }
    
    
    func mapping(map: Map) {
        
        totalCount <- map["total_count"]
        incomplete_results <- map["incomplete_results"]
        items <- map["items"]
        pulls <- map["user"]
    }
    
    
}



